﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///This base class is to define a person
///<summary>
///Date last modified: 06/10/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework
{
    class Person
    {
        private string firstName, secondName;

        //Getters, setters and validation code
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("First name field cannot be blank!");
                firstName = value;
            }
        }

        public string SecondName
        {
            get
            {
                return secondName;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("Second name field cannot be blank!");
                secondName = value;
            }
        }
    }
}
