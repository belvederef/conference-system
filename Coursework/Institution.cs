﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///This class is to define an institution
///<summary>
///Date last modified: 11/10/2016


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework
{
    class Institution
    {
        private string institutionName, institutionAddress;

        //Getters and setters
        public string InstitutionName
        {
            get
            {
                return institutionName;
            }
            set
            {
                institutionName = value;
            }
        }

        public string InstitutionAddress
        {
            get
            {
                return institutionAddress;
            }
            set
            {
                institutionAddress = value;
            }
        }
    }
}
