﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///This class is to initialise the Invoice window only 
///<summary>
///Date last modified: 17/10/2016


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {
        public Invoice()
        {
            InitializeComponent();
        }

        internal Invoice(Attendee attendee)
        {
            InitializeComponent();
            Show();
            lblInvName2.Content = attendee.FirstName + " " + attendee.SecondName;
            lblInvInstitution2.Content = attendee.institute.InstitutionName;
            lblInvConference2.Content = attendee.ConferenceName;
            lblInvCost2.Content = Convert.ToString(attendee.GetCost());
        }
        
    }
}
