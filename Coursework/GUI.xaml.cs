﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///This class is to define the GUI behaviour
///<summary>
///Date last modified: 17/10/2016


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;




namespace Coursework
{
    /// <summary>
    /// Interaction logic for GUI.xaml
    /// </summary>
    public partial class GUI : Window
    {
        Attendee attendee = new Attendee();
        
        public GUI()
        {
            InitializeComponent();
        }

        //Takes the values contained in the text boxes, checkboxes and combobox and updates the instance of the Attendee class properties.
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            attendee.institute.InstitutionName = txtInstitution.Text;
            attendee.institute.InstitutionAddress = txtInstitutionAdd.Text;
            attendee.Paid = (Boolean)chkPaid.IsChecked;
            attendee.Presenter = (Boolean)chkPresenter.IsChecked;

            //Try to get first name. If data is invalid, throw an error
            try
            {
                attendee.FirstName = txtName1.Text;
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }

            //Try to get second name. If data is invalid, throw an error
            try
            {
                attendee.SecondName = txtName2.Text;
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }

            //Try to get attendance reference number. If data is invalid, throw an error
            try
            {
                attendee.AttendeeRef = Convert.ToInt32(txtAttendee.Text);
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }

            //Try to get the conference name. If data is invalid, throw an error
            try
            {
                attendee.ConferenceName = txtConference.Text;
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }

            //Try to get the registration type. Can only be "full", "student" or "organiser". If data is invalid, throw an error
            try
            {
                attendee.RegistrationType = Convert.ToString(cboxRegistration.SelectedItem);  //set highlighted combobox value
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }

            //Try to get the paper title. If data is invalid, throw an error
            try
            {
                attendee.PaperTitle = txtPaper.Text;
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }


        //Uses the values of the Attendee class methods to update all the box values
        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            txtName1.Text = attendee.FirstName;
            txtName2.Text = attendee.SecondName;
            txtAttendee.Text = Convert.ToString(attendee.AttendeeRef);
            txtInstitution.Text = attendee.institute.InstitutionName;
            txtInstitutionAdd.Text = attendee.institute.InstitutionAddress;
            txtConference.Text = attendee.ConferenceName;
            cboxRegistration.SelectedValue = attendee.RegistrationType;
            txtPaper.Text = attendee.PaperTitle;
            chkPaid.IsChecked = attendee.Paid;
            chkPresenter.IsChecked = attendee.Presenter;
        }


        //Removes the contents of the boxes (does not change any properties of the class instance)
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtName1.Clear();
            txtName2.Clear();
            txtAttendee.Clear();
            txtInstitution.Clear();
            txtInstitutionAdd.Clear();
            txtConference.Clear();
            cboxRegistration.SelectedIndex = -1;
            txtPaper.Clear();
            chkPaid.IsChecked = false;
            chkPresenter.IsChecked = false;
        }


        //Produces an invoice when the "invoice" button is clicked
        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            Invoice invoice = new Invoice(attendee);
        }


        //Adds entries to the combobox
        private void cboxRegistration_Initialized(object sender, EventArgs e)
        {
            cboxRegistration.Items.Add("Full");
            cboxRegistration.Items.Add("Student");
            cboxRegistration.Items.Add("Organiser");
        }


        //Show certificate when "certificate" button is clicked
        private void btnCertificate_Click(object sender, RoutedEventArgs e)
        {
            Certificate certificate = new Certificate(attendee);
        }
    }
}
