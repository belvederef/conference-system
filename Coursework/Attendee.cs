﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///This class is to define an attendee. It inherites some properties from the Person class and 
///makes use of validation code to ensure data input is in a correct format.
///Included are getCost() method to find out the amount to be paid and
///Disc() method to calculate 10% discount where applies
///<summary>
///Date last modified: 06/10/2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework
{
    class Attendee : Person
    {
        private int attendeeRef;
        private string conferenceName, registrationType, paperTitle;
        private Boolean paid, presenter;
        public Institution institute = new Institution();

        //Getters and setters for safe access of private properties. When data does not match validation, an error is thrown.
        public int AttendeeRef
        {
            get 
            { 
                return attendeeRef; 
            }
            set 
            {
                if (value < 40000 || value > 60000)
                    throw new ArgumentException("Not in range!\nAttendee reference must be \nbetween 40,000 and 60,000!");
                attendeeRef = value; 
            }
        }

        public string ConferenceName
        {
            get 
            {
                return conferenceName; 
            }
            set 
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("Conference name field cannot be blank!");
                conferenceName = value; 
            }
        }

        public string RegistrationType
        {
            get
            {
                return registrationType;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("Registration type field cannot be blank!");
                registrationType = value;
            }
        }

        public string PaperTitle
        {
            get 
            { 
                return paperTitle; 
            }
            //Field must not be blank if presenter is set to true. Should be blank if presenter is set to false.
            set
            {
                if (presenter == true && string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("Paper title field cannot be blank if you are presenting!");
                if (presenter != true && !string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("Paper title must be blank if you are not presenting!");
                paperTitle = value; 
            }
        }

        public Boolean Paid
        {
            get { return paid; }
            set { paid = value; }
        }

        public Boolean Presenter
        {
            get { return presenter; }
            set { presenter = value; }
        }



        //Returns a value discounted of 10%
        public double Disc(double value)
        {
            return value = value - (value / 100 * 10);
        }

        // Will return the cost of attending the conference for this Attendee
        //If the attendee is a presenter then they receive a 10% discount
        public double GetCost()
        {
            if (registrationType == "Full" && presenter == true)
                return Disc(500);
            else if (registrationType == "Full" && presenter != true)
                return 500;
            if (registrationType == "Student" && presenter == true)
                return Disc(300);
            else if (registrationType == "Student" && presenter != true)
                return 300;
            else return 0;
        }
    }
}
