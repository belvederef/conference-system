﻿/// Author: Francesco Belvedere, 40218080
///<summary>
///This class is to initialise the Certificate window only 
///<summary>
///Date last modified: 17/10/2016


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework
{
    /// <summary>
    /// Interaction logic for Certificate.xaml
    /// </summary>
    public partial class Certificate : Window
    {
        public Certificate()
        {
            InitializeComponent();
        }

        internal Certificate(Attendee attendee)
        {
            InitializeComponent();
            Show();
            lblCertificate.Content = "This is to certify that " + attendee.FirstName +
                "\n" + attendee.SecondName + " attended " + attendee.ConferenceName;
            if (attendee.Presenter == true)
                lblCertificate.Content += "\nand presented a paper entitled \n'" + attendee.PaperTitle + "'";
        }
    }
}
